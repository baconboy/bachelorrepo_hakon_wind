﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSmove : MonoBehaviour
{
    public CharacterController controller;

    public float startspeed = 12f;
    private float speed;
    public float runspeed = 15f;
    public float gravity = -9.81f;
    public float jumpheight = 3f;

    public Transform groundcheck;
    public float grounddistance = 0.4f;
    public LayerMask groundmask;

    Vector3 velocity;

    bool isgrounded;

    void Update()
    {
        speed = startspeed;
        if(Input.GetKey(KeyCode.LeftShift))
        {
            speed = runspeed;
        }
        


        isgrounded = Physics.CheckSphere(groundcheck.position, grounddistance, groundmask);
        if(isgrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (Input.GetButtonDown("Jump") && isgrounded)
        {
            velocity.y = Mathf.Sqrt(jumpheight * -2f * gravity);
        }

    }
}
