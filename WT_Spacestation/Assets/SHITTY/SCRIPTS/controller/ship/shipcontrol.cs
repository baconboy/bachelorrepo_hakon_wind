﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipcontrol : MonoBehaviour
{
    public GameObject RightHoverbox;




    public bool vehicleentered;
    public float forwardSpeed = 25f, strafespeed = 7.5f, hoverspeed = 5f;
    private float activeforwardspeed, activestrafespeed, activehoverspeed;

    public float lookrotatespeed = 90f;
    private Vector2 lookinput, screenCenter, mousedistance;

    private float rollInput;
    public float rollspeed = 90f, rollacceleration = 3.5f;

    private float forwardacceleration = 2.5f, staveacceleration = 2f, hoveracceleration = 2f; 
    // Start is called before the first frame update
    void Start()
    {
        screenCenter.x = Screen.width *.5f;
        screenCenter.y = Screen.height *.5f;
    }

    void Update()
    {
        if (vehicleentered)
        {
            lookinput.x = Input.mousePosition.x;
            lookinput.y = Input.mousePosition.y;

            mousedistance.x = (lookinput.x - screenCenter.x) / screenCenter.x;
            mousedistance.y = (lookinput.y - screenCenter.y) / screenCenter.y;

            mousedistance = Vector2.ClampMagnitude(mousedistance, 1f);

            transform.Rotate(-mousedistance.y * lookrotatespeed * Time.deltaTime, mousedistance.x * lookrotatespeed * Time.deltaTime, rollInput * rollspeed * Time.deltaTime, Space.Self);

            rollInput = Mathf.Lerp(rollInput, Input.GetAxisRaw("Roll"), rollacceleration * Time.deltaTime);


            activeforwardspeed = Mathf.Lerp(activeforwardspeed, Input.GetAxisRaw("Vertical") * forwardSpeed, forwardacceleration * Time.deltaTime);
            activestrafespeed = Mathf.Lerp(activestrafespeed, Input.GetAxisRaw("Horizontal") * strafespeed, staveacceleration * Time.deltaTime);
            activehoverspeed = Mathf.Lerp(activehoverspeed, Input.GetAxisRaw("Hover") * hoverspeed, hoveracceleration * Time.deltaTime);

            transform.position += transform.forward * activeforwardspeed * Time.deltaTime;
            transform.position += (transform.right * activestrafespeed * Time.deltaTime) + (transform.up * activehoverspeed * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.F))
            {
                vehicleentered = false;
            }
        }

        else
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                vehicleentered = true;
            }

        }
        trackinput();
    }
    private void trackinput()
    {
        if (mousedistance.y > 0)
        {
            Debug.Log("y ist größer als");
        }
        if (mousedistance.y < 0)
        {
            Debug.Log("y ist kleiner als");
        }

        if (mousedistance.x > 0)
        {
            Debug.Log("x ist größer als");

        }
        if (mousedistance.x < 0)
        {
            Debug.Log("x ist kleiner als");
            
        }
    }
}
