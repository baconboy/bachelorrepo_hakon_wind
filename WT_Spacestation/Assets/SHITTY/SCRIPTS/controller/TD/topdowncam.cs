﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class topdowncam : MonoBehaviour
{
    private float movespeed;
    public float startmovementspeed = 5f;
    public float runspeed;
    public float rotationspeed = 10f;
    public Camera mycamera;

    private void Start()
    {
    }


    private void Update()
    {

        if(Input.GetKey(KeyCode.LeftShift))
        {
            movespeed = runspeed;
        }
        else
        {
            movespeed = startmovementspeed;
        }
        /////////////////////////
        ///nicht von mir
        ////////////////////////

        var input = new Vector3(-Input.GetAxisRaw("Horizontal"),0, -Input.GetAxisRaw("Vertical"));
        Vector3 velocity = input.normalized * movespeed;
        transform.position += velocity * Time.deltaTime;

        ///////////////////////////
        ///
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (mycamera.fieldOfView > 1)
            {
                mycamera.fieldOfView--;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (mycamera.fieldOfView < 100)
            {
                mycamera.fieldOfView++;
            }
        }
        handlerotation();
    }
    private void handlerotation()
    {
        if (Input.GetKey(KeyCode.E))
        {
            this.transform.RotateAroundLocal(Vector3.up, rotationspeed);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            this.transform.RotateAroundLocal(-Vector3.up, rotationspeed);
        }

    }

}
//transform.RotateAround(this.transform.position, this.transform.up, rotationspeed* Time.deltaTime);
