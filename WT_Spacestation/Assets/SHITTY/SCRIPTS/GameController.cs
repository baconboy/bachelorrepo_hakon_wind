using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject FPsetup;
    public GameObject TDsetup;
    public GameObject rdsetup;

    public GameObject Roof;
    public GameObject UI_menu;
    public float mousemultiplyer;

    // Start is called before the first frame update
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
            {

            UI_menu.SetActive(!UI_menu.activeSelf);

            if(Cursor.visible)
            {
                Cursor.visible = false;
              
            }

            if (!Cursor.visible)
            {
                Cursor.visible = true;
            }

        }
    }

    public void FD_active()
    {
        FPsetup.SetActive(true);
        TDsetup.SetActive(false);
        rdsetup.SetActive(false);
        Roof.SetActive(false);

    }
    public void TD_active()
    {
        FPsetup.SetActive(false);
        TDsetup.SetActive(true);
        rdsetup.SetActive(false);
        Roof.SetActive(false);
    }
    public void rd_active()
    {
        FPsetup.SetActive(false);
        TDsetup.SetActive(false);
        rdsetup.SetActive(true);
        Roof.SetActive(false);

    }
}
